var keythereum = require("keythereum");
var secp256k1 = require("secp256k1/elliptic");
const Store = require('./store.js');
const store = new Store({
    configName: 'user-secrets',
    defaults: {
        keys: { }
    }
  });
class KeyCreator {
    constructor() { }
    create() {
        var dk = keythereum.create().privateKey;
        var privateKey = dk.toString('hex');
        var publicKey = secp256k1.publicKeyCreate(dk, false).slice(1).toString('hex');
        store.set('keys', { privateKey, publicKey });
    }
  }
  
  // expose the class
  module.exports = KeyCreator;