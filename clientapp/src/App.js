import React, { Component } from 'react';
import { Modal, Button } from 'antd';
import logo from './logo.svg';
import 'antd/dist/antd.css';
import './App.css';


class App extends Component {
  state = { visible: false }
  showModal = () => {
    this.setState({
      visible: true,
    });
  }
  handleOk = (e) => {
    const electron = window.require("electron");
    const { BrowserWindow } = electron.remote;
    let win = BrowserWindow.getFocusedWindow();
    win.loadURL("http://www.google.com");
    console.log(e);
    this.setState({
      visible: false,
    });
  }
  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
          <Button type="primary" onClick={this.showModal}>Open</Button>
          <Modal
          title="Basic Modal"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <p>Some contents...</p>
          <p>Some contents...</p>
          <p>Some contents...</p>
        </Modal>
        </p>
      </div>
    );
  }
}

export default App;
